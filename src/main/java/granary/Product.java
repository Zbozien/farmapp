package granary;

public enum Product {
    pszenica(5),
    żyto(4),
    mięso(20),
    truskawki(7),
    marchewki(5),
    ogórki(5),
    arbuzy(10),
    pomidory(5),
    sałata(5),
    kapusta(9),
    mleko(2),
    jajka(1),
    ser(20);

    private int price;

    Product(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrize(int price) {
        this.price = price;
    }

}
