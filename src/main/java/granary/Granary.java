package granary;

import java.util.HashMap;
import java.util.Map;

public class Granary {
    private Map<Product, Integer> products;

    public Granary() {
        this.products = new HashMap<>();
    }

    public void addProduct(Product product, int quantity) {
        if (products.containsKey(product)) {

            products.merge(product, quantity, (prev, next) -> prev + next);

        } else {
            products.put(product, quantity);
        }

    }
    public void harvest(){
        for (Product product: Product.values()) {
            int quantity = (int)Math.ceil(Math.random()*10);
            addProduct(product,quantity);
        }
    }

    public void removeProduct(Product product, int quantity) {
        if (products.containsKey(product) && products.get(product) >= quantity) {
            products.merge(product, quantity, (prev, next) -> prev - next);
        } else{
            throw new RuntimeException("You can not remove this product");
        }
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }

}
