package granary;

import java.util.Scanner;

public class GranaryController {
    private Granary granary;

    void begin(){
        granary = new Granary();
        granary.harvest(); // cos na start
        handleCommands();
    }

    void handleCommands(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Co chcesz zrobic?");
        String option = scanner.nextLine();

        switch (option){
            case "harvest":
                granary.harvest();
                System.out.println("Udało sie zebrać plon");
                break;
            case "showProducts":
                System.out.println(granary.getProducts());
                break;
            case "sprzedaj":

                System.out.println("Sprzedałeś produkty");
            default:
                System.out.println("komenda nieprawidłowa");
        }

        handleCommands();

    }

}
